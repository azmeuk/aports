# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwin
pkgver=5.23.2
pkgrel=1
pkgdesc="An easy to use, but flexible, composited Window Manager"
# armhf blocked by qt5-qtdeclarative
# s390x, mips64 and riscv64 blocked by kscreenlocker
arch="all !armhf !s390x !mips64 !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-only"
depends="
	kirigami2
	qt5-qtmultimedia
	qt5-qtwayland
	xwayland
	"
depends_dev="
	breeze-dev
	eudev-dev
	fontconfig-dev
	kactivities-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdeclarative-dev
	kdecoration-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kidletime-dev
	kinit-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kpackage-dev
	krunner-dev
	kscreenlocker-dev
	kservice-dev
	ktextwidgets-dev
	kwayland-dev
	kwayland-server-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	lcms2-dev
	libdrm-dev
	libepoxy-dev
	libinput-dev
	libqaccessibilityclient-dev
	libxi-dev
	libxkbcommon-dev
	mesa-dev
	mesa-gbm
	pipewire-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtscript-dev
	qt5-qtsensors-dev
	qt5-qtx11extras-dev
	wayland-dev
	xcb-util-cursor-dev
	xcb-util-image-dev
	xcb-util-wm-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	qt5-qttools-dev
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwin-$pkgver.tar.xz
	0001-only-use-gldrawbuffer-with-desktop-gl.patch
	"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd

	# kwin_wayland has CAP_SYS_NICE set. Because of this, libdbus doesn't trust the
	# environment and ignores it, causing for example keyboard shortcuts to not work
	# Remove CAP_SYS_NICE from kwin_wayland to make them work again
	setcap -r "$pkgdir"/usr/bin/kwin_wayland
}
sha512sums="
d7062504d26a3fe6db5d8d34486d450035613a8d5f986a074aeda8e22acde06e2dbfde17daccd8199a2f3a7855d1e891c92af7478150e7f320b592143aca4ebd  kwin-5.23.2.tar.xz
98860ebc4ab4102240d32260ae88a9e1d27bc8470da7c6c786e3e7808ea9c518297c41e43745d62da79827b92a388e636c6f5c0dedf8dc238e74da0f4cbbd055  0001-only-use-gldrawbuffer-with-desktop-gl.patch
"
